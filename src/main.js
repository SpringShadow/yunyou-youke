import Vue from 'vue'
import App from './App'

import '@/styles/common.scss'
// 配置项
const config = require('./config/config.js');
Vue.prototype.$config = config
// 封装uni的一些方法
import './plugins/uniPromise.js'
// 过滤器
import './plugins/filter.js'
// 封装的storage
import './plugins/storage.js'
// uni-vuex
let vuexStore = require("@/store/$u.mixin.js");
Vue.mixin(vuexStore);
import store from '@/store';
// uview-ui
import './plugins/uview.js'
// 封装的分享相关方法
let mpShare = require('uview-ui/libs/mixin/mpShare.js');
Vue.mixin(mpShare)

Vue.config.productionTip = false
App.mpType = 'app'

const app = new Vue({
    store,
    ...App
})
app.$mount()
