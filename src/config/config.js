// production   development
let env='production'
let baseUrl,appid,tmplIds,domainPath
if(env === 'production'){
    console.log('生产环境')
    baseUrl='https://yypay.reoyoo.com';
    appid='wxb04b88bea84e84b9';
    tmplIds='7cs4-SS48slCAGIDVfg4GWOBHbdJasqbshIbtDL8PGc';
    domainPath=['https://pic1.reoyoo.com','https://yy.reoyoo.com','https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com']
}else{
    console.log('开发环境')
    baseUrl='https://testyypay.reoyoo.com';
    appid='wxb6ce0cb854b4899a';
    tmplIds='pWsNbc0dTB2aVs9mUCKHAeWC6KWWp80owHp2LScYTpM';
    domainPath=['https://testyy.reoyoo.com','https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com','https://pic1.reoyoo.com']
}
let config = {
    tokenKey:'X-Token',
	// 请求接口的地址
	baseUrl: baseUrl,
    // 小程序appid
    appid:appid,
	// 是否需要MD5加密数据
	isMd5: true,
    // 订阅消息模板id
    tmplIds:tmplIds,
    // 图片下载安全域名
    domainPath:domainPath
}

module.exports = config;
