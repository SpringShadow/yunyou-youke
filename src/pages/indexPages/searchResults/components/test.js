export const testData = [
    {
        photoid: 11388,
        url: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201116/1/202011161343423220fe7a09e41f67c8_t.jpg",
        image_url_t: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201116/1/202011161343423220fe7a09e41f67c8_t.jpg",
        image_url_c: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201116/1/202011161343423220fe7a09e41f67c8_c.jpg",
        price: 0.01,
        isSelected: false,
    },
    {
        photoid: 11361,
        url: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201109/1/202011090931577912d80c7121d71fc0_t.jpg",
        image_url_t: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201109/1/202011090931577912d80c7121d71fc0_t.jpg",
        image_url_c: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201109/1/202011090931577912d80c7121d71fc0_c.jpg",
        price: 0.01,
        isSelected: false,
    },
    {
        photoid: 11357,
        url: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/20201108172739860138dc51bf47a12b_t.jpg",
        image_url_t: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/20201108172739860138dc51bf47a12b_t.jpg",
        image_url_c: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/20201108172739860138dc51bf47a12b_c.jpg",
        price: 0.01,
        isSelected: false,
    },
    {
        photoid: 11356,
        url: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/202011081727398993034507e5de2ec7_t.jpg",
        image_url_t: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/202011081727398993034507e5de2ec7_t.jpg",
        image_url_c: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/202011081727398993034507e5de2ec7_c.jpg",
        price: 0.01,
        isSelected: false,
    },
    {
        photoid: 11355,
        url: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/2020110817274010410acbdc09306b82_t.jpg",
        image_url_t: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/2020110817274010410acbdc09306b82_t.jpg",
        image_url_c: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/2020110817274010410acbdc09306b82_c.jpg",
        price: 0.01,
        isSelected: false,
    },
    {
        photoid: 11354,
        url: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/2020110817274006549bc5eb6f2fdf96_t.jpg",
        image_url_t: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/2020110817274006549bc5eb6f2fdf96_t.jpg",
        image_url_c: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/2020110817274006549bc5eb6f2fdf96_c.jpg",
        price: 0.01,
        isSelected: false,
    },
    {
        photoid: 11353,
        url: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/2020110817274011394e88a70fb79016_t.jpg",
        image_url_t: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/2020110817274011394e88a70fb79016_t.jpg",
        image_url_c: "https://reoyoo-album.obs.cn-south-1.myhuaweicloud.com/images/test/20201108/1/2020110817274011394e88a70fb79016_c.jpg",
        price: 0.01,
        isSelected: false,
    }
    
]
