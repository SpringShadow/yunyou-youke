import Vue from 'vue'
import {
    toFixed,
    thousandBitSeparator,
} from '../common/utils.js'
export const filters = {
    toFixed: toFixed,
    thousandBitSeparator: thousandBitSeparator,
}

Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})
