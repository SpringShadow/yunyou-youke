import Vue from 'vue'
let uniPromise = {};
// 登录
uniPromise.login = function() {
    let promise = new Promise((resolve, reject) => {
        uni.login({
            provider: 'weixin',
            success: (loginRes) => {
                if (loginRes) {
                    resolve(loginRes)
                } else {
                    resolve('')
                }
            },
            fail: () => {
                reject();
            }
        });
    })
    return promise
}
// 获取用户信息
uniPromise.getUserInfo = function() {
    let promise = new Promise((resolve, reject) => {
        uni.getUserInfo({
            provider: 'weixin',
            lang: 'zh_CN',
            success: (infoRes) => {
                if (infoRes) {
                    resolve(infoRes)
                } else {
                    resolve('')
                }
            },
            fail: () => {
                reject()
            }
        })
    })
    return promise
}
// 获取设备信息
uniPromise.getSystemInfo = function() {
    let promise = new Promise((resolve, reject) => {
        uni.getSystemInfo({
            success: (res) => {
                resolve(res)
            },
            fail: () => {
                reject()
            }
        });
    })
    return promise
}
// 获取元素布局信息
uniPromise.getEleInfo = function(ele, vm) {
    let promise = new Promise((resolve, reject) => {
        const query = uni.createSelectorQuery().in(vm);
        query
            .select(ele)
            .boundingClientRect(data => {
                resolve(data)
            })
            .exec();
    })
    return promise
}
// 拍照
uniPromise.takePhoto = function() {
    let promise = new Promise((resolve, reject) => {
        const ctx = uni.createCameraContext();
        ctx.takePhoto({
            quality: 'high',
            success: res => {
                if (res.tempImagePath) {
                    resolve(res.tempImagePath);
                }
            }
        });
    })
    return promise
}
// 选择相片
uniPromise.chooseImage = function(data = {}) {
    let promise = new Promise((resolve, reject) => {
        uni.chooseImage({
            count: data.count || 9,
            sizeType: data.sizeType || ['original', 'compressed'],
            sourceType: data.sourceType || ['album'],
            success: res => {
                resolve(res)
            },
            fail: res => {

            }
        });
    })
    return promise
}
// 下载文件
uniPromise.downloadFile = function(data = {}) {
    let promise = new Promise((resolve, reject) => {
        uni.downloadFile({
            url: data.url,
            success: res => {
                resolve(res)
            },
            fail: res => {
                reject()
            }
        });
    })
    return promise
}
// 保存图片道相册
uniPromise.saveImageToPhotosAlbum = function(data = {}) {
    let promise = new Promise((resolve, reject) => {
        uni.saveImageToPhotosAlbum({
            filePath: data.filePath,
            success: res => {
                resolve(res)
            },
            fail: res => {
                console.log(res);
                reject()
            }
        });
    })
    return promise
}
// 显示模态框
uniPromise.showModal = function(option) {
    let promise = new Promise((resolve, reject) => {
        let obj = {
            success(res) {
                resolve(res)
            },
            fail() {
                reject()
            }
        }
        uni.showModal(Object.assign(obj, option));
    })
    return promise
}
// 获取用户设置
uniPromise.getSetting = function(withSubscriptions = false) {
    let promise = new Promise((resolve, reject) => {
        uni.getSetting({
            withSubscriptions: withSubscriptions,
            success(res) {
                resolve(res)
            },
            fail() {
                reject()
            }
        })
    })
    return promise
}
// 打开用户设置
uniPromise.openSetting = function() {
    let promise = new Promise((resolve, reject) => {
        uni.openSetting({
            success(res) {
                resolve(res)
            },
            fail() {
                reject()
            }
        });
    })
    return promise
}
// 订阅消息
uniPromise.requestSubscribeMessage = function(tmplIds) {
    let promise = new Promise((resolve, reject) => {
        uni.requestSubscribeMessage({
            tmplIds: [tmplIds],
            success(res) {
                resolve(res);
            },
            fail() {
                reject()
            }
        });
    })
    return promise
}
// 微信支付
uniPromise.requestPayment = function(option) {
    let promise = new Promise((resolve, reject) => {
        let obj = {
            success(res) {
                resolve(res)
            },
            fail() {
                reject()
            }
        }
        wx.requestPayment(Object.assign(obj, option));
    })
    return promise
}
// 设置页面标题
uniPromise.setTitle = function(title) {
    uni.setNavigationBarTitle({
        title: title
    });
}

Vue.prototype.$uniPromise = uniPromise;

Vue.prototype.$showToast = function(mes) {
    if (mes && typeof mes == 'string') {
        uni.showToast({
            title: mes,
            icon: 'none',
            duration: 2000
        });
    }
}

Vue.prototype.$showLoading = function(mes = '上传中', isMask = true) {
    uni.showLoading({
        title: mes,
        mask: isMask
    });
}

Vue.prototype.$hideLoading = function(mes = '上传中', isMask = true) {
    uni.hideLoading();
}
