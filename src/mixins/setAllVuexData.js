import {
    getVuex_templateData
} from '@/common/common.js';
export const setAllVuexData = {
    methods: {
        setAllVuexData(worksDetail, templateDetail, worksId, musicUrl) {
            // 模板基础信息
            let baseInfo = {
                id: worksDetail.tempid,
                code: worksDetail.code,
                name: worksDetail.name,
                coverImage: worksDetail.cover_url
            };
            this.$u.vuex('vuex_templateData', baseInfo);
            // 获取图片数据 增加一些属性
            let list = this.getVuex_selectedPhotosList(worksDetail.photos);
            this.$u.vuex('vuex_selectedPhotosList', list);
            // 模板详情数据
            let data = getVuex_templateData(templateDetail);
            this.$u.vuex('vuex_templateData', this.$u.deepMerge(this.vuex_templateData, data));
            // 设置作品id
            this.$u.vuex('vuex_worksId', worksId);
            // 设置音乐
            if (worksDetail.music_url) {
                this.$u.vuex('vuex_music', {
                    url: worksDetail.music_url
                });
            }else{
                this.$u.vuex('vuex_music', {
                    url: ''
                });
            }
            // 设置作品是否被点赞
            this.$u.vuex('vuex_worksLikeStatus', worksDetail.isPraise);
            
        },
        // 获取被选中的图片
        getVuex_selectedPhotosList(list) {
            let _list = JSON.parse(JSON.stringify(list));
            _list.forEach((item, index) => {
                item.isSelected = true;
                item.selectedIndex = index + 1;
                item.imageIndex = index;
            });
            return _list;
        },
    }
}
