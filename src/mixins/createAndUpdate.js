import { createWorks, editWorks } from '@/api/works.js';
export const createAndUpdate = {
    methods: {
        // 创建作品
        async createWorks() {
            let album_list = this.getAlbumList(this.vuex_selectedPhotosList);
            let workData = {
                tempid: this.vuex_templateData.id,
                name: this.vuex_templateData.name,
                cover_url: this.vuex_templateData.coverImage,
                album_list: JSON.stringify(album_list),
                music_id: this.vuex_music.id || 0
            };
            let data = await createWorks(workData);
            if (data && data.worksid) {
                this.$u.vuex('vuex_worksId', data.worksid);
            }
        },

        getAlbumList(list) {
            let album_list = [];
            list.forEach(item => {
                album_list.push({
                    albumid: item.albumid
                });
            });
            return album_list
        },
        // 编辑作品
        async editWorks() {
            let album_list = this.getAlbumList(this.vuex_selectedPhotosList);
            let workData = {
                worksid: this.vuex_worksId,
                name: this.vuex_templateData.name,
                album_list: JSON.stringify(album_list),
                music_id: this.vuex_music.id || 0
            };
            let data = await editWorks(workData);
        }
    }
}
