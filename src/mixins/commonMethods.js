import {
    getSalesInfo,
    getCouponList,
} from '@/api/sales.js';
import {
    computeAmount
} from '@/api/order.js';

export const commonMethods = {
    data() {
        return {
            // 促销信息
            tipsList: [],
            // 是否全部选中照片
            isSelectdAll: false,
            // 打包价格
            packPrice: 0,
            // 结算信息
            billingInfo: {
                // 总价
                saleAmount: 0,
                // 实际支付价
                payAmount: 0,
                // 优惠金额
                discount: 0,
                // 选择照片数量
                hadSelectPicCount: 0
            },
            // 是否显示优惠券详情
            showCouponDetail: false,
            // 优惠券列表
            couponList: [],
            // 使用的优惠券
            selectedCoupon: {},
            // 选中的照片
            selectedPics: {}
        };
    },
    methods: {
        // 获取顶部促销信息数据
        async getSalesInfo(userid) {
            let saleInfo = await getSalesInfo({
                userid: userid
            });
            this.tipsList = saleInfo.Tips;
            this.packPrice = saleInfo.PackPrice;
        },
        // 获取优惠券信息
        async getCouponList(userid, state = 0) {
            this.couponList = getCouponList({
                userid: userid,
                state: state
            });
            if (this.couponList.length > 0) {
                this.selectedCoupon =
                    this.couponList.filter(item => {
                        return item.isCanUse;
                    })[0] || {};
            }
        },
        //获取结算页面 是否全部选中的状态
        getSelectAllStatus(isSelectdAll) {
            this.isSelectdAll = isSelectdAll;
            uni.$emit('updateSelectdAllStatus', {
                status: isSelectdAll
            });
        },
        // 查看优惠券详情
        viewCouponDetail() {
            this.showCouponDetail = true;
        },
        // 关闭优惠券详情
        closeCoupon() {
            this.showCouponDetail = false;
        },
        getPayParam(payData, userid, couponNumber) {
            return {
                sale_amount: payData.packagedPrice,
                count: payData.hadSelectPicCount,
                ids: payData.ids,
                type: payData.type,
                userid: userid,
                coupon_number: couponNumber || ''
            };
        },
        // 计算价格
        async computeAmount(param) {
            let data = await computeAmount(param);
            if (res.Data) {
                let _data = res.Data;
                this.billingInfo.payAmount = _data.pay_amount;
                this.billingInfo.saleAmount = this.selectedPics.saleAmount;
                this.billingInfo.discount = accAdd(param.sale_amount, -_data.pay_amount);
                this.billingInfo.hadSelectPicCount = param.count;
                this.billingInfo.ids = this.selectedPics.ids;
                this.billingInfo.type = this.selectedPics.type;
            }
        },

    }
}
