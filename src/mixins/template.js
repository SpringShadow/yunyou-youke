import { uploadFile } from '@/common/utils.js';
import { getItemIndex } from '@/common/common.js';
export const template = {
    data() {
        return {
            // 当前选择的图片的className
            activeClassName: '',
            // 图片宽高等信息
            photoEleInfo: {},
            // 额外的图标数组
            iconArr: [],
            // 照片背景底图
            modeArr: []
        };
    },
    props: {
        // 空白数据的列表 用于预览模板页面
        emptyList: {
            type: Array,
            default: function() {
                return [];
            }
        },
        // 预览模板（templete）预览作品(preview) 自己查看作品(view) 制作（make） 被查看作品（分享出去给其他人查看）(shareView)
        templateStatus: {
            type: String,
            default: ''
        }
    },
    created() {
        uni.$on('hideMenu', data => {
            this.activeClassName = '';
        });
        this.iconArr = this.vuex_templateData.iconArr;
        this.modeArr = this.vuex_templateData.modeArr;
    },
    mounted() {},
    methods: {
        // 是否显示一些小的图标
        isShowIcon(icon) {
            for (let i = 0; i < this.iconArr.length; i++) {
                if (this.iconArr[i].icon.includes(icon)) {
                    return true;
                }
            }
        },
        // 获取图标的地址
        getIconPath(icon) {
            for (let i = 0; i < this.iconArr.length; i++) {
                if (this.iconArr[i].icon.includes(icon)) {
                    return this.iconArr[i].url;
                }
            }
        },
        // 获取每个图片的底图
        getModePath(mode) {
            for (let i = 0; i < this.modeArr.length; i++) {
                if (this.modeArr[i].mode.includes(mode)) {
                    return this.modeArr[i].url;
                }
            }
        },
        // 点击列表其他部分 隐藏删除和替换按钮
        hideMenu() {
            if (this.templateStatus === 'make') {
                this.activeClassName = '';
            }
        },

        // 显示编辑图片的按钮
        async showEditIcon(className) {
            // 如果只有是自己制作页面才显示编辑按钮
            if (this.templateStatus === 'make') {
                this.activeClassName = className;
                this.photoEleInfo = await this.$uniPromise.getEleInfo(`.${className}`, this);
            }
        },
        // 删除照片
        delect(item) {
            console.log(item);
            // 如果是默认图片的话 提示不能删除
            if (item.isDefault) {
                this.$showToast('默认图片不可删除');
                return;
            }
            if (this.vuex_selectedPhotosList.length <= 1) {
                this.$showToast('请至少保留一张照片');
                return;
            }
            let index = getItemIndex(this.vuex_selectedPhotosList, item, 'albumid');
            let list = JSON.parse(JSON.stringify(this.vuex_selectedPhotosList));
            list.splice(index, 1);
            this.activeClassName = '';
            this.$u.vuex('vuex_selectedPhotosList', list);
        },
        // 选择照片进行上传
        async choosePhoto(item) {
            let photo = await this.$uniPromise.chooseImage({ count: 1 });
            photo && photo.tempFilePaths.length > 0 && this.startUpload(photo.tempFilePaths[0], item);
        },
        // 开始上传照片
        startUpload(path, item) {
            setTimeout(() => {
                this.$showLoading();
            }, 500);
            uploadFile(`${this.$config.baseUrl}/api/upload/obs`, path, this.$config.tokenKey, {}, true)
                .then(res => {
                    if (res.RequestStatus === 100 && res.Data) {
                        this.replacePhoto(res.Data, item);
                    }
                    this.$hideLoading();
                })
                .catch(err => {
                    this.$hideLoading();
                });
        },
        // 替换照片
        replacePhoto(target, item) {
            console.log(target);
            console.log(item);
            let list = JSON.parse(JSON.stringify(this.vuex_selectedPhotosList));
            target.isSelected = true;
            target.id = target.albumid;
            if (item.isDefault) {
                target.selectedIndex = list.length + 1;
                target.imageIndex = list.length - 1;
                list.push(target);
            } else {
                target.selectedIndex = item.selectedIndex;
                target.imageIndex = item.imageIndex;
                let index = getItemIndex(this.vuex_selectedPhotosList, item);
                list[index] = target;
            }
            this.$u.vuex('vuex_selectedPhotosList', list);
        }
    },
    watch: {
        emptyList: {
            handler(newVal, oldVal) {
                this.getList();
            },
            immediate: true,
            deep: true
        },
        vuex_selectedPhotosList: {
            handler(newVal, oldVal) {
                if (newVal.length > 0) {
                    this.getList();
                }
            },
            deep: true
        }
    },
}
