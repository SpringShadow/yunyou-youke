
export const viewBigImage = {
    data() {
        return {
            canvasWidth: 0,
            canvasHeight: 0,
            maskImg: '../static/img/searchResults/watermark.png',
            canvasId: 'canvasId'
        };
    },
    methods: {
        // 预览照片
        gotoPreView(currentImage) {
            if (currentImage.maskImage) {
                this.showBigImage(currentImage.maskImage);
            } else {
                uni.showLoading({
                    title: '图片加载中',
                    mask: true
                });
                this.getImageInfo(currentImage.image_url_c)
                    .then(image => {
                        this.canvasWidth = image.width;
                        this.canvasHeight = image.height;
                        return this.downLoadImage(currentImage.image_url_c);
                    })
                    .then(res => {
                        return this.drawImage(res.tempFilePath, this.canvasId);
                    })
                    .then(res => {
                        return this.getCacheImage(this.canvasWidth, this.canvasHeight, this.canvasId);
                    })
                    .then(res => {
                        uni.hideLoading();
                        currentImage.maskImage = res.tempFilePath;
                        this.showBigImage(currentImage.maskImage);
                    });
            }
        },
        // 获取网络图片的宽高信息
        getImageInfo(path) {
            let promise = new Promise((resolve, reject) => {
                uni.getImageInfo({
                    src: path,
                    success: image => {
                        resolve(image);
                    },
                    fail: res => {
                        reject(res);
                    }
                });
            });
            return promise;
        },
        // 下载网络照片到本地
        downLoadImage(path) {
            let promise = new Promise((resolve, reject) => {
                uni.downloadFile({
                    url: path,
                    success: res => {
                        resolve(res);
                    },
                    fail: res => {
                        reject(res);
                    }
                });
            });
            return promise;
        },
        // 绘制合成图片
        drawImage(path, canvasID) {
            let promise = new Promise((resolve, reject) => {
                const ctx = wx.createCanvasContext(canvasID);
                ctx.save();
                ctx.drawImage(path, 0, 0, this.canvasWidth, this.canvasHeight);
                ctx.drawImage(this.maskImg, 0, 0, this.canvasWidth, this.canvasHeight);
                ctx.restore();
                ctx.draw(true, () => {
                    resolve();
                });
            });
            return promise;
        },
        // 将canvas合成照片转为图片
        getCacheImage(width, height, canvasID) {
            let promise = new Promise((resolve, reject) => {
                uni.canvasToTempFilePath(
                    {
                        x: 0,
                        y: 0,
                        width: width,
                        height: height,
                        destWidth: width,
                        destHeight: height,
                        canvasId: canvasID,
                        success: res => {
                            resolve(res);
                        },
                        fail: res => {}
                    }
                    
                );
            });
            return promise;
        },
        // 显示大图
        showBigImage(path) {
            let list = [];
            list.push(path);
            uni.previewImage({
                current: 0,
                urls: list
            });
        }
    }
}
