import uniRequest from './request.js'
import Vue from 'vue'

// 获取作品列表
function getWorksList(data) {
    return uniRequest({
        url: '/api/tourist/works',
        method: 'post',
        data: data
    })
}
// 获取作品详情
function getWorksDetail(worksId) {
    return uniRequest({
        url: `/api/tourist/works/detail?worksid=${worksId}`,
        method: 'post',
    })
}
// 创建作品
function createWorks(data) {
    return uniRequest({
        url: '/api/tourist/works/create',
        method: 'post',
        data: data
    })
}
// 编辑作品
function editWorks(data) {
    return uniRequest({
        url: '/api/tourist/works/edit',
        method: 'post',
        data: data
    })
}
// 删除作品  POST 
function delectWorks(worksId) {
    return uniRequest({
        url: `/api/tourist/works/del?worksid=${worksId}`,
        method: 'post',
    })
}

export {
    getWorksList,
    getWorksDetail,
    createWorks,
    editWorks,
    delectWorks
}
