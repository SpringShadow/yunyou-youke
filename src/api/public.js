import uniRequest from './request.js'
import Vue from 'vue'
// 记录分享次数
function recordShare(data) {
    return uniRequest({
        url: '/api/tourist/pub/works/share',
        method: 'post',
        data: data
    })
}
// 记录查看次数
function getWorksDetail(data) {
    return uniRequest({
        url: '/api/tourist/pub/works/detail',
        method: 'post',
        data: data
    })
}
// 记录点赞次数
function recordLike(data) {
    return uniRequest({
        url: '/api/tourist/pub/works/praise',
        method: 'post',
        data: data
    })
}

export {
    recordShare,
    getWorksDetail,
    recordLike
}
