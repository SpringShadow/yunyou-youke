import uniRequest from './request.js'

// 获取所有商家
function getAllMerchant(data) {
    return uniRequest({
        url: '/api/TouristService/GetAllMerchant',
        method: 'post',
        data: data
    })
}
// 获取当前商家
function getCurrMerchant(data) {
    return uniRequest({
        url: '/api/TouristService/GetCurrMerchant',
        method: 'post',
        data: data
    })
}

export {
    getAllMerchant,
    getCurrMerchant,
}
