import uniRequest from './request.js'
import Vue from 'vue'

// 获取促销信息
function getSalesInfo(data) {
    return uniRequest({
        url: '/api/TouristService/GetSaleInfo',
        method: 'post',
        data: data
    })
}
// 获取优惠券列表
function getCouponList(data) {
    return uniRequest({
        url: '/api/tourist/coumpon/list',
        method: 'post',
        data: data
    })
}

export {
    getSalesInfo,
    getCouponList
}
