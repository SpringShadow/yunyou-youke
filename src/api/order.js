import uniRequest from './request.js'
import Vue from 'vue'
// 计算订单金额
function computeAmount(data) {
    return uniRequest({
        url: '/api/tourist/v2/compute/amount',
        method: 'post',
        data: data
    })
}
// 创建订单
function createOrder(data) {
    return uniRequest({
        url: '/api/tourist/v2/create/order',
        method: 'post',
        data: data
    })
}

export {
    computeAmount,
    createOrder
}
