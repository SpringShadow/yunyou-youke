import uniRequest from './request.js'
import Vue from 'vue'
 
// 获取模板列表
function getTemplateList(data) {
    return uniRequest({
        url: '/api/tourist/poster/temp/list',
        method: 'post',
        data: data
    })
}
// 获取模板详情
function getTemplateDetail(tempId=0) {
    return uniRequest({
        url: `/api/tourist/pub/poster/temp/detail?tempid=${tempId}`,
        method: 'post',
    })
}


export {
    getTemplateList,
    getTemplateDetail,
}
