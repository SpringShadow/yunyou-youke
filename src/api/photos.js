import uniRequest from './request.js'
import Vue from 'vue'
// 待购买照片
function getNobuyData(data) {
    return uniRequest({
        url: '/api/tourist/v2/photo/nobuy',
        method: 'post',
        data: data
    })
}
// 已购买照片
function getHadBuyPhotos(data) {
    return uniRequest({
        url: '/api/tourist/v2/photo',
        method: 'post',
        data: data
    })
}
// 删除待购买照片
function deletePhotoes(data) {
    return uniRequest({
        url: '/api/TouristService/DeletePhotoes',
        method: 'post',
        data: data
    })
}
// 已经上传照片
function getUploadPhotoes(data) {
    return uniRequest({
        url: '/api/tourist/v2/upload/photo/list',
        method: 'post',
        data: data
    })
}
// 获取相册照片
function getAlbumPhotoes(data) {
    return uniRequest({
        url: '/api/tourist/album',
        method: 'post',
        data: data
    })
}
// 删除相册照片
function deleteAlbumPhoto(data) {
    return uniRequest({
        url: `/api/tourist/album/delete`,
        method: 'post',
        data: data
    })
}
export {
    getNobuyData,
    getHadBuyPhotos,
    deletePhotoes,
    getUploadPhotoes,
    getAlbumPhotoes,
    deleteAlbumPhoto
}
