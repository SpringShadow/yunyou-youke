import uniRequest from './request.js'
import Vue from 'vue'

// 获取推荐音乐列表
function getMusicList() {
    return uniRequest({
        url: '/api/tourist/music/list',
        method: 'post',
        // data: data
    })
}
// 获取收藏音乐列表
function getCollectMusicList(data) {
    return uniRequest({
        url: '/api/tourist/music/collect/list',
        method: 'post',
        data: data
    })
}
// 获取作品详情
function collectMusic(musicId) {
    return uniRequest({
        url: `/api/tourist/music/collect?musicid=${musicId}`,
        method: 'post',
    })
}
export {
    getMusicList,
    getCollectMusicList,
    collectMusic
}
