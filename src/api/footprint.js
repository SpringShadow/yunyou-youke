import uniRequest from './request.js'
import Vue from 'vue'

// 获取打卡足迹
function getFootprint(data) {
    return uniRequest({
        url: '/api/tourist/footmart',
        method: 'post',
        data: data
    })
}
// 创建打卡足迹
function createFootprint(data) {
    return uniRequest({
        url: '/api/tourist/footmart/create',
        method: 'post',
        data: data
    })
}

export {
    getFootprint,
    createFootprint
}
