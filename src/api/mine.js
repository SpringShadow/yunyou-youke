import uniRequest from './request.js'
import Vue from 'vue'
// 获取用户信息
function getUserInfo(data) {
    return uniRequest({
        url: '/api/tourist/info',
        method: 'post',
        data: data
    })
}
// 编辑用户签名
function editSignature(data) {
    return uniRequest({
        url: '/api/tourist/profile/set',
        method: 'post',
        data: data
    })
}
// 获取自助上传照片的code
function getCode(data) {
    return uniRequest({
        url: '/api/TouristService/GetCode',
        method: 'post',
        data: data
    })
}
// 绑定自助上传照片的code
function bindCode(data) {
    return uniRequest({
        url: '/api/tourist/v2/bind/code',
        method: 'post',
        data: data
    })
}

export {
    getUserInfo,
    editSignature,
    getCode,
    bindCode,
}
