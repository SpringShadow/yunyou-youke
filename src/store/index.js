import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

let lifeData = {};

try {
    // 尝试获取本地是否存在lifeData变量，第一次启动APP时是不存在的
    lifeData = uni.getStorageSync('lifeData');
} catch (e) {

}
// 'vuex_selectedPhotosList','vuex_templateData','vuex_worksId'
// 需要永久存储，且下次APP启动需要取出的，在state中的变量名
let saveStateKeys = ['vuex_nickName', 'vuex_avatarUrl', 'vuex_sessionKey', 'vuex_merchantId', 'vuex_merchantName',
    'vuex_userId', 'vuex_merchantMode', 'vuex_openid'
];

// 保存变量到本地存储中
const saveLifeData = function(key, value) {
    // 判断变量名是否在需要存储的数组中
    if (saveStateKeys.includes(key)) {
        // 获取本地存储的lifeData对象，第一次打开APP，不存在lifeData变量，故放一个{}空对象
        let lifeData = uni.getStorageSync('lifeData') || {};
        lifeData[key] = value;
        // 执行这一步后，所有需要存储的变量，都挂载在本地的lifeData对象中
        uni.setStorageSync('lifeData', lifeData);
    }
}
const store = new Vuex.Store({
    state: {
        // 如果上面从本地获取的lifeData对象下有对应的属性，就赋值给state中对应的变量
        // 加上vuex_前缀，是防止变量名冲突，也让人一目了然
        vuex_nickName: lifeData.vuex_nickName || '请登录',
        vuex_avatarUrl: lifeData.vuex_avatarUrl || '../../../static/tabBar/mine/defaultAvatar.png',
        vuex_sessionKey: lifeData.vuex_sessionKey || '',
        vuex_openid: lifeData.vuex_openid || '',
        // 用户编辑的个人签名
        vuex_userSignature: '',
        // 授权
        vuex_hadAuthorize: false,
        //订阅消息
        vuex_subscribeMessage: '',
        // 商家id
        vuex_merchantId: lifeData.vuex_merchantId || '',
        // 商家名称
        vuex_merchantName: lifeData.vuex_merchantName || '',
        // 商家模板code
        vuex_merchantMode: lifeData.vuex_merchantMode || '',
        // 进入刷脸页面次数
        vuex_enterFacePageTimes: 0,
        // 刷脸结果数据
        vuex_searchResultsList: [],
        // 用户id
        vuex_userId: lifeData.vuex_userId || '',
        // 待购买照片页面的用户id
        vuex_tobePaidUserId: '',
        // 待购买页面数据
        vuex_toBePaidList: [],
        // 预览我的相册列表
        vuex_myPhotosList: [],
        // 选择制作分享海报的相片
        vuex_selectedPhotosList: [],
        // 模板数据
        vuex_templateData: {},
        // 选择的music
        vuex_music: {},
        // 正在预览的作品id
        vuex_worksId: '',
        // 作品被点赞的状态
        vuex_worksLikeStatus: false
    },
    mutations: {
        $uStore(state, payload) {
            // 判断是否多层级调用，state中为对象存在的情况，诸如user.info.score = 1
            let nameArr = payload.name.split('.');
            let saveKey = '';
            let len = nameArr.length;
            if (nameArr.length >= 2) {
                let obj = state[nameArr[0]];
                for (let i = 1; i < len - 1; i++) {
                    obj = obj[nameArr[i]];
                }
                obj[nameArr[len - 1]] = payload.value;
                saveKey = nameArr[0];
            } else {
                // 单层级变量，在state就是一个普通变量的情况
                state[payload.name] = payload.value;
                saveKey = payload.name;
            }
            // 保存变量到本地，见顶部函数定义
            saveLifeData(saveKey, state[saveKey])
        }
    }
})

export default store
