// 获取index
export function getItemIndex(arr, target, key) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i][key] == target[key]) return i;
    }
    return -1;
}

/**
 * 数组映射
 * @param 模板数组
 * @param 待转换数组
 * @param 需要替换和填充的数据
 * @return 映射之后的数组
 */

export function mapList(template, maplist, fillPath = '') {
    if (template.length > 0 && maplist.length > 0) {
        let list = [];
        let _index = -1;
        for (let i = 0; i < template.length; i++) {
            if (_index >= maplist.length - 1) {
                return list
            }
            list.push([]);
            for (let j = 0; j < template[i]; j++) {
                _index += 1;
                if (_index < maplist.length) {
                    list[i].push([]);
                    let temp = maplist[_index];
                    temp.imageIndex = _index;
                    list[i][j] = temp;
                } else {
                    if (j > 0) {
                        for (let k = j; k < template[i]; k++) {
                            console.log(222);
                            // 判断下一张或多张是不是和图片列表的最后一张在同一个模板里
                            let defaultPhoto = {};
                            defaultPhoto.image_url_c = fillPath;
                            defaultPhoto.imageIndex = _index;
                            defaultPhoto.isDefault = true;
                            list[i][j] = defaultPhoto;
                        }
                    }
                }
            }
        }
        return list;
    } else {
        return []
    }
}
// 转换模板详情数据
export function getVuex_templateData(data) {
    let obj = {};
    obj.iconArr = [];
    obj.modeArr = [];
    let list = [];
    data.forEach(item => {
        if (item.code === 'theme') {
            obj.theme = item.image_url;
        }
        if (item.code.includes('icon')) {
            let temp = {};
            temp.icon = item.code;
            temp.url = item.image_url;
            obj.iconArr.push(temp);
        }
        if (item.code.includes('mode')) {
            let temp = {};
            temp.mode = item.code;
            temp.url = item.image_url;
            obj.modeArr.push(temp);
        }
    });
    return obj
}
